<?php

namespace DS\CaptainHookExtension\Git\Command;

use PHPUnit\Framework\TestCase;

class GetBranchNameTest extends TestCase
{
    public function testGetCommand()
    {
        $root = random_int(0, 99999);
        $command = new GetBranchName($root);
        $expected = "git -C '$root' branch | grep \\* | cut -d ' ' -f2";

        $this->assertEquals($expected, $command->getCommand());
    }
}

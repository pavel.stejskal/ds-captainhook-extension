<?php

namespace DS\CaptainHookExtension\Hook\Condition\Branch;

use CaptainHook\App\Console\IO\NullIO;
use PHPUnit\Framework\TestCase;
use SebastianFeldmann\Cli\Command\Result as CmdResult;
use SebastianFeldmann\Cli\Command\Runner\Result;
use SebastianFeldmann\Cli\Command\Runner\Simple;
use SebastianFeldmann\Git\Repository;

class NotBranchTest extends TestCase
{

    public function testCurrentBranchIsNotIgnored()
    {
        $runnerMock = $this->createMock(Simple::class);
        $cmdResult = new CmdResult('get branch', 0, 'testing');
        $runnerResult = new Result($cmdResult);
        $runnerMock->method('run')->willReturn($runnerResult);

        $condition = new NotBranch(['production'], $runnerMock);
        $this->assertTrue($condition->isTrue(new NullIO(), new Repository()));
    }

    public function testCurrentBranchIsIgnored()
    {
        $runnerMock = $this->createMock(Simple::class);
        $cmdResult = new CmdResult('get branch', 0, 'gtr/MR-123/feature');
        $runnerResult = new Result($cmdResult);
        $runnerMock->method('run')->willReturn($runnerResult);

        $condition = new NotBranch(['production'], $runnerMock);
        $this->assertTrue($condition->isTrue(new NullIO(), new Repository()));
    }
}

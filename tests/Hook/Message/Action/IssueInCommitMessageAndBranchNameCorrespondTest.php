<?php

namespace DS\CaptainHookExtension\Hook\Message\Action;

use CaptainHook\App\Config;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Exception\ActionFailed;
use Exception;
use PHPUnit\Framework\TestCase;
use SebastianFeldmann\Cli\Command\Result as CmdResult;
use SebastianFeldmann\Cli\Command\Runner\Result;
use SebastianFeldmann\Cli\Command\Runner\Simple;
use SebastianFeldmann\Git\CommitMessage;
use SebastianFeldmann\Git\Repository;

class IssueInCommitMessageAndBranchNameCorrespondTest extends TestCase
{

    /**
     * @throws Exception
     */
    public function testPassing(): void
    {
        $runnerMock = $this->createMock(Simple::class);
        $cmdResult = new CmdResult('get branch', 0, 'gtf/MM-4434/feature');
        $runnerResult = new Result($cmdResult);
        $runnerMock->method('run')->willReturn($runnerResult);

        $config = new Config('');
        $io = new IO\NullIO();
        $repo = new Repository();
        $repo->setCommitMsg(new CommitMessage('[MM-4434][FTR] Some commit message'));
        $action = new Config\Action('commit-msg');

        $this->expectNotToPerformAssertions();

        $tested = new IssueInCommitMessageAndBranchNameCorrespond($runnerMock);
        $tested->execute($config, $io, $repo, $action);
    }

    /**
     * @throws Exception
     */
    public function testFailing(): void
    {
        $runnerMock = $this->createMock(Simple::class);
        $cmdResult = new CmdResult('get branch', 0, 'gtf/MM-4434/hotfix');
        $runnerResult = new Result($cmdResult);
        $runnerMock->method('run')->willReturn($runnerResult);

        $config = new Config('');
        $io = new IO\NullIO();
        $repo = new Repository();
        $repo->setCommitMsg(new CommitMessage('[MM-2222][FTR] Some commit message'));
        $action = new Config\Action('commit-msg');

        $this->expectException(ActionFailed::class);

        $tested = new IssueInCommitMessageAndBranchNameCorrespond($runnerMock);
        $tested->execute($config, $io, $repo, $action);
    }
}

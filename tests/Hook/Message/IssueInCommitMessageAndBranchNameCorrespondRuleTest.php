<?php

namespace DS\CaptainHookExtension\Hook\Message;

use PHPUnit\Framework\TestCase;
use SebastianFeldmann\Git\CommitMessage;

class IssueInCommitMessageAndBranchNameCorrespondRuleTest extends TestCase
{

    /**
     * @dataProvider dataProvider
     * @param string      $commitMessageRegex
     * @param int         $commitMessageGroup
     * @param string      $branchRegex
     * @param int         $branchGroup
     * @param string      $branch
     * @param string      $content
     * @param bool        $expected
     * @param string|null $expectedHint
     */
    public function testPass(
        string $commitMessageRegex,
        int $commitMessageGroup,
        string $branchRegex,
        int $branchGroup,
        string $branch,
        string $content,
        bool $expected,
        string $expectedHint = ''
    ): void {
        $rule = new IssueInCommitMessageAndBranchNameCorrespondRule(
            $branch,
            $commitMessageRegex,
            $commitMessageGroup,
            $branchRegex,
            $branchGroup
        );

        $pass = $rule->pass(new CommitMessage($content));
        $this->assertEquals($expected, $pass);
        if (!$pass) {
            $this->assertEquals($expectedHint, $rule->getHint());
        }
    }

    public function dataProvider(): array
    {
        return [
            'issue number is same' => [
                'commitMessageRegex' => "/(\\[([A-Z]{2,4}-\\d{1,5})\\])?(\\[(REF|WIP|FIX|FTR|SCR|CLN|TST|DEP)\\]) .+/",
                'commitMessageGroup' => 2,
                'branchRegex' => "/\w+\/([A-Z]{2,4}-\d{1,5})\/?.*/",
                'branchGroup' => 1,
                'branch' => 'gtr/MR-3374/feature',
                'content' => '[MR-3374][FTR] Velmi good',
                'expected' => true
            ],
            'issue number is different' => [
                'commitMessageRegex' => "/(\\[([A-Z]{2,4}-\\d{1,5})\\])?(\\[(REF|WIP|FIX|FTR|SCR|CLN|TST|DEP)\\]) .+/",
                'commitMessageGroup' => 2,
                'branchRegex' => "/\w+\/([A-Z]{2,4}-\d{1,5})\/?.*/",
                'branchGroup' => 1,
                'branch' => 'gtr/MR-3374/feature',
                'content' => '[MR-123][FTR] Velmi good',
                'expected' => false,
                'expectedHint' => 'Issue "MR-123" in commit does not correspond with issue "MR-3374" in branch name'
            ],
            'branch name does not contain issue number' => [
                'commitMessageRegex' => "/(\\[([A-Z]{2,4}-\\d{1,5})\\])?(\\[(REF|WIP|FIX|FTR|SCR|CLN|TST|DEP)\\]) .+/",
                'commitMessageGroup' => 2,
                'branchRegex' => "/\w+\/([A-Z]{2,4}-\d{1,5})\/?.*/",
                'branchGroup' => 1,
                'branch' => 'gtr/develop',
                'content' => '[MR-123][FTR] Velmi good',
                'expected' => true
            ]
        ];
    }
}

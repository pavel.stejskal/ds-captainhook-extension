<?php

namespace DS\CaptainHookExtension\Hook;

use CaptainHook\App\Config;
use CaptainHook\App\Console\IO\NullIO;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use SebastianFeldmann\Cli\Command\Result as CmdResult;
use SebastianFeldmann\Cli\Command\Runner\Simple;
use SebastianFeldmann\Cli\Processor;
use SebastianFeldmann\Git\Operator\Index;
use SebastianFeldmann\Git\Repository;

class CodeSnifferTest extends TestCase
{

    /**
     * @dataProvider provideData
     */
    public function testExecute($stagedFiles, $actionOptions, $expectedCommand)
    {
        $runner = $this->createRunnerMock($expectedCommand);
        $repository = $this->createRepositoryMock($stagedFiles);

        $config = new Config('');
        $io = new NullIO();
        $action = new Config\Action('commit-msg', $actionOptions);

        $codeSnifferAction = new CodeSniffer($runner);
        $codeSnifferAction->execute($config, $io, $repository, $action);
    }

    /**
     * @param string $expectedCommand
     * @return Simple
     */
    protected function createRunnerMock(string $expectedCommand)
    {
        $cmdResult = new CmdResult('', 0, '');

        $processorMock = $this->createMock(Processor::class);
        $processorMock->expects($this->once())
            ->method('run')
            ->with($this->equalTo($expectedCommand))
            ->willReturn($cmdResult);


        return new Simple($processorMock);
    }

    /**
     * @param array $stagedFiles
     * @return MockObject|Repository
     */
    protected function createRepositoryMock(array $stagedFiles)
    {
        $indexMock = $this->createMock(Index::class);
        $indexMock->method('getStagedFilesOfType')->willReturn($stagedFiles);

        $repositoryMock = $this->createMock(Repository::class);
        $repositoryMock->expects($this->once())
            ->method('getIndexOperator')
            ->willReturn($indexMock);

        return $repositoryMock;
    }

    public function provideData()
    {
        return [
            'With default options' => [
                'stagedFiles' => [
                    'fileA.php',
                    'fileB.php'
                ],
                'actionOptions' => [],
                'expectedCommand' => "bin/phpcs 'fileA.php' 'fileB.php'"
            ],
            'With overrided cmd' => [
                'stagedFiles' => [
                    'fileA.php'
                ],
                'actionOptions' => [
                    'cmd' => 'bin/phpcs.phar',
                ],
                'expectedCommand' => "bin/phpcs.phar 'fileA.php'"
            ],
            'With additional options' => [
                'stagedFiles' => [
                    'fileA.php'
                ],
                'actionOptions' => [
                    'options' => [
                        '-p' => null,
                        '--standard' => 'phpcs.xml'
                    ]
                ],
                'expectedCommand' => "bin/phpcs -p --standard='phpcs.xml' 'fileA.php'"
            ],
            'With regex filter' => [
                'stagedFiles' => [
                    'src/fileA.php',
                    'app/fileA.php',
                    'tests/fileA.php',
                ],
                'actionOptions' => [
                    'regex' => '/src\/.*/'
                ],
                'expectedCommand' => "bin/phpcs 'src/fileA.php'"
            ],
        ];
    }
}

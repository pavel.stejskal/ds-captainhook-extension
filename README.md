A collection of useful actions to be used with [CaptainHook](https://github.com/CaptainHookPhp/captainhook).

## Install

```bash
composer require --dev ds/captainhook-extension
```

## Actions

#### Run PHPCS on staged files
```json
    // captainhook.json
    // ... 
    "commit-msg": {
        "enabled": true,
        "actions": [
            {
                "action": "\\DS\\CaptainHookExtension\\Hook\\CodeSniffer",
                "options": {
                    // default values
                    "cmd": "bin/phpcs"
                    "options": []
                    "regex": "/.*/",
                    "limit": null
                }
            }
        ]
    }
    // ...
```
- cmd - phpcs command to run
- options - array of options to pass to command
- regex - regex pattern to filter changed files
- limit - maximum number of files to check (null means check all)

#### Check issue number in commit message corresponds with branch name

```json
    // captainhook.json
    // ... 
    "commit-msg": {
        "enabled": true,
        "actions": [
            {
                "action": "\\DS\\CaptainHookExtension\\Hook\\Message\\Action\\IssueInCommitMessageAndBranchNameCorrespond",
                "options": {
                    // default values
                    "branchCmd": "git branch | grep \\* | cut -d ' ' -f2"
                    "commitMessageRegex": "/(\\[([A-Z]{2,4}-\\d{1,5})\\])?(\\[(REF|WIP|FIX|FTR|SCR|CLN|TST|DEP)\\]) .+/"
                    "commitMessageGroup": 2
                    "branchRegex": "/\w+\/([A-Z]{2,4}-\d{1,5})\/?.*/",
                    "branchGroup": 1
                }
            }
        ]
    }
    // ...
```
- branchCmd - bash command to get name of branch
- commitMessageRegex - regex to parse commit message
- commitMessageGroup - capturing group for issue commitMessageRegex
- branchRegex - regex to parse branch name
- branchGroup - capturing group for issue in branchRegex

## Conditions

#### Ignore hook when you are on some branch
```json
    // captainhook.json
    // ...
    "pre-push": {
        "enabled": true,
        "actions": [
            {
                "action": "...",
                "conditions": [
                    {
                        "exec": "\\DS\\CaptainHookExtension\\Hook\\Condition\\Branch\\NotBranch",
                        "args": [
                            ["testing","test","staging-new"]
                        ]
                    }
                ]
            }
        ]
    }
    // ...
```
- args - array of branch names to make this condition fail

## Contributing
Before commiting changes, make sure to check and test your changes with 
```bash
composer check
composer test
```

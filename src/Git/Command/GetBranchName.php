<?php

namespace DS\CaptainHookExtension\Git\Command;

use SebastianFeldmann\Git\Command\Base;

/**
 * Class GetBranchName
 */
class GetBranchName extends Base
{

    /**
     * Return the command to execute.
     *
     * @return string
     * @throws \RuntimeException
     */
    protected function getGitCommand(): string
    {
        return "branch | grep \\* | cut -d ' ' -f2";
    }
}

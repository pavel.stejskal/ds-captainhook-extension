<?php

namespace DS\CaptainHookExtension\Hook\Message;

use CaptainHook\App\Hook\Message\Rule;
use SebastianFeldmann\Git\CommitMessage;

class IssueInCommitMessageAndBranchNameCorrespondRule extends Rule\Base
{

    /**
     * @var string
     */
    private $branch;

    /**
     * @var string
     */
    private $commitMessageRegex;

    /**
     * @var int
     */
    private $commitMessageGroup;

    /**
     * @var string
     */
    private $branchRegex;

    /**
     * @var int
     */
    private $branchGroup;

    /**
     * @param string $branch
     * @param string $commitMessageRegex
     * @param int    $commitMessageGroup
     * @param string $branchRegex
     * @param int    $branchGroup
     */
    public function __construct(
        string $branch,
        string $commitMessageRegex,
        int $commitMessageGroup,
        string $branchRegex,
        int $branchGroup
    ) {
        $this->branch = $branch;
        $this->commitMessageRegex = $commitMessageRegex;
        $this->commitMessageGroup = $commitMessageGroup;
        $this->branchRegex = $branchRegex;
        $this->branchGroup = $branchGroup;
    }

    /**
     * Checks if a commit message passes the rule.
     *
     * @param CommitMessage $msg
     * @return bool
     */
    public function pass(CommitMessage $msg): bool
    {
        $branchIssue = $this->getIssueFromBranch($this->branch);
        $commitIssue = $this->getIssueFromCommitMessage($msg);

        if ($branchIssue !== null && $branchIssue !== $commitIssue) {
            $this->hint = sprintf(
                'Issue "%s" in commit does not correspond with issue "%s" in branch name',
                $commitIssue,
                $branchIssue
            );

            return false;
        }

        return true;
    }

    /**
     * @param CommitMessage $msg
     * @return string|null
     */
    private function getIssueFromCommitMessage(CommitMessage $msg): ?string
    {
        return $this->getMatchingRegexGroup($this->commitMessageRegex, $msg->getContent(), $this->commitMessageGroup);
    }

    /**
     * @param string $branch
     * @return string|null
     */
    private function getIssueFromBranch(string $branch): ?string
    {
        return $this->getMatchingRegexGroup($this->branchRegex, $branch, $this->branchGroup);
    }

    /**
     * Matches $subject with $regex and returns matching capturing $group, null otherwise
     *
     * @param string $regex   Regex to match subject against
     * @param string $subject Subject
     * @param int    $group   Capturing group to return
     * @return string|null
     */
    private function getMatchingRegexGroup(string $regex, string $subject, int $group): ?string
    {
        if (preg_match($regex, $subject, $matches)) {
            return $matches[$group] ?? null;
        }

        return null;
    }
}

<?php

namespace DS\CaptainHookExtension\Hook\Message\Action;

use CaptainHook\App\Config;
use CaptainHook\App\Config\Action;
use CaptainHook\App\Config\Options;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Hook\Message\Action\Book;
use CaptainHook\App\Hook\Message\RuleBook;
use DS\CaptainHookExtension\Git\Command\GetBranchName;
use DS\CaptainHookExtension\Hook\Message\IssueInCommitMessageAndBranchNameCorrespondRule;
use Exception;
use SebastianFeldmann\Cli\Command\Runner;
use SebastianFeldmann\Cli\Command\Runner\Simple;
use SebastianFeldmann\Git\Repository;

/**
 * Class IssueInCommitMessageAndBranchNameCorrespond
 */
class IssueInCommitMessageAndBranchNameCorrespond extends Book
{

    /**
     * @var Runner
     */
    private $runner;

    /**
     * @param Runner|null $runner
     */
    public function __construct(?Runner $runner = null)
    {
        $this->runner = $runner ?? new Simple();
    }

    /**
     * Execute the configured action
     *
     * @param Config     $config
     * @param IO         $io
     * @param Repository $repository
     * @param Action     $action
     * @return void
     * @throws Exception
     */
    public function execute(Config $config, IO $io, Repository $repository, Action $action): void
    {
        $options = $action->getOptions();
        $book = new RuleBook();
        $book->addRule($this->createRule($options, $repository));

        $this->validate($book, $repository, $io);
    }

    /**
     * @param Options $options
     * @param Repository $repository
     * @return IssueInCommitMessageAndBranchNameCorrespondRule
     */
    private function createRule(
        Options $options,
        Repository $repository
    ): IssueInCommitMessageAndBranchNameCorrespondRule {
        $branch = $this->getBranch($repository);
        $commitMessageRegex = $options->get(
            'commitMessageRegex',
            "/(\\[([A-Z]{2,4}-\\d{1,5})\\])?(\\[(REF|WIP|FIX|FTR|SCR|CLN|TST|DEP)\\]) .+/"
        );
        $commitMessageGroup = $options->get('commitMessageGroup', 2);
        $branchRegex = $options->get('branchRegex', "/\w+\/([A-Z]{2,4}-\d{1,5})\/?.*/");
        $branchGroup = $options->get('branchGroup', 1);

        return new IssueInCommitMessageAndBranchNameCorrespondRule(
            $branch,
            $commitMessageRegex,
            $commitMessageGroup,
            $branchRegex,
            $branchGroup
        );
    }

    /**
     * @param Repository $repository
     * @return string
     */
    private function getBranch(Repository $repository): string
    {
        $command = new GetBranchName($repository->getRoot());
        $result = $this->runner->run($command);

        return trim($result->getStdOut());
    }
}

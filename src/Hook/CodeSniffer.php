<?php

namespace DS\CaptainHookExtension\Hook;

use CaptainHook\App\Config;
use CaptainHook\App\Console\IO;
use CaptainHook\App\Exception\ActionFailed;
use CaptainHook\App\Hook\Action;
use SebastianFeldmann\Cli\Command\Executable;
use SebastianFeldmann\Cli\Command\Runner;
use SebastianFeldmann\Cli\Command\Runner\Simple;
use SebastianFeldmann\Git\Repository;

class CodeSniffer implements Action
{

    /**
     * @var Runner
     */
    private $runner;

    /**
     * @param Runner|null $runner
     */
    public function __construct(?Runner $runner = null)
    {
        $this->runner = $runner ?? new Simple();
    }

    /**
     * Executes the action
     *
     * @param Config $config
     * @param IO $io
     * @param Repository $repository
     * @param Config\Action $action
     * @throws ActionFailed
     */
    public function execute(Config $config, IO $io, Repository $repository, Config\Action $action): void
    {
        $actionOptions = $action->getOptions();
        $command = $actionOptions->get('cmd', 'bin/phpcs');
        $options = $actionOptions->get('options', []);
        $regex = $actionOptions->get('regex', '/.*/');
        $limit = $actionOptions->get('limit');

        $filesToCheck = $this->getFilesToCheck($repository, $regex);

        if (empty($filesToCheck)) {
            $io->write('No files to check', true, IO::VERBOSE);
            return;
        }

        if ($limit !== null && count($filesToCheck) > $limit) {
            $io->write('Too many files, skip', true, IO::VERBOSE);
            return;
        }

        $command = new Executable($command);
        foreach ($options as $key => $value) {
            $command->addOption($key, $value);
        }

        $io->write('checking files:');
        foreach ($filesToCheck as $file) {
            $io->write('  - ' . $file, true, IO::VERBOSE);
            $command->addArgument($file);
        }

        $result = $this->runner->run($command);

        if (!$result->isSuccessful()) {
            throw new ActionFailed($result->getStdOut() . PHP_EOL . $result->getStdErr());
        }

        $io->write($result->getStdOut());
    }

    /**
     * @param Repository $repository
     * @param string $regex
     * @return array
     */
    protected function getFilesToCheck(Repository $repository, string $regex): array
    {
        $changedPHPFiles = $repository->getIndexOperator()->getStagedFilesOfType('php');

        return preg_grep($regex, $changedPHPFiles);
    }
}

<?php

namespace DS\CaptainHookExtension\Hook\Condition\Branch;

use CaptainHook\App\Console\IO;
use CaptainHook\App\Hook\Condition;
use DS\CaptainHookExtension\Git\Command\GetBranchName;
use SebastianFeldmann\Cli\Command\Runner;
use SebastianFeldmann\Cli\Command\Runner\Simple;
use SebastianFeldmann\Git\Repository;

class NotBranch implements Condition
{
    /**
     * @var string[]
     */
    private $branches;

    /**
     * @var Runner
     */
    private $runner;

    /**
     * @param array $branches
     * @param Runner|null $runner
     */
    public function __construct(array $branches, ?Runner $runner = null)
    {
        $this->branches = $branches;
        $this->runner = $runner ?? new Simple();
    }

    /**
     * Evaluates a condition
     *
     * @param IO $io
     * @param Repository $repository
     * @return bool
     */
    public function isTrue(IO $io, Repository $repository): bool
    {
        $currentBranch = $this->getCurrentBranch($repository);

        return !in_array($currentBranch, $this->branches, true);
    }

    /**
     * @param Repository $repository
     * @return string
     */
    private function getCurrentBranch(Repository $repository): string
    {
        $command = new GetBranchName($repository->getRoot());
        $result = $this->runner->run($command);

        return $result->getStdOut();
    }
}
